import { APIKey, APIToken } from "./secretData.js";


export default function createAList(listName, boardId) {
    fetch(`https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${APIKey}&token=${APIToken}`, {
        method: 'POST'
    });
}