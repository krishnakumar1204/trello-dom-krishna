import { APIKey, APIToken } from "./secretData.js";


export default function createCard(listId, cardName) {
    fetch(`https://api.trello.com/1/cards?idList=${listId}&name=${cardName}&key=${APIKey}&token=${APIToken}`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json'
        }
    })
}