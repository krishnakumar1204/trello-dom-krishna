import { APIKey, APIToken } from "./secretData.js";

export default function createABoard(boardName) {
  fetch(
    `https://api.trello.com/1/boards/?name=${boardName}&key=${APIKey}&token=${APIToken}`,
    {
      method: "POST",
    }
  )
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        return new Error("Response in NOT ok.");
      }
    })
    .catch((error) => {
      return error;
    });
}
