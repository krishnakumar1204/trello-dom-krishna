import { APIKey, APIToken } from "./secretData.js";

export default function deleteCard(cardId) {
    return fetch(`https://api.trello.com/1/cards/${cardId}?key=${APIKey}&token=${APIToken}`, {
        method: 'DELETE'
    })
        .then((response) => {
            if (response.ok) {
                return response.json();
            } else {
                return new Error("Response in NOT ok.");
            }
        })
        .catch((error) => {
            return error;
        });
}