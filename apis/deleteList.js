import { APIKey, APIToken } from "./secretData.js";

export default function deleteList(listId) {

    fetch(`https://api.trello.com/1/lists/${listId}?key=${APIKey}&token=${APIToken}&closed=true`, {
        method: 'PUT'
    });
}