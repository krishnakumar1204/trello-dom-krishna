import { APIKey, APIToken } from "./secretData.js";


export default function getInitialBoards() {
  return fetch(
    `https://api.trello.com/1/members/me/boards?key=${APIKey}&token=${APIToken}`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
      },
    }
  )
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        return new Error("Response in NOT ok.");
      }
    })
    .catch((error) => {
      return error;
    });
}
