// 1. Create a function getBoard which takes the boardId as arugment and returns a promise which resolves with board data

import { APIKey, APIToken } from "./secretData.js";

export default function getBoard(boardId) {
  return fetch(
    `https://api.trello.com/1/boards/${boardId}?key=${APIKey}&token=${APIToken}`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
      },
    }
  )
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        return new Error("Response in NOT ok.");
      }
    })
    .catch((error) => {
      return error;
    });
}

// module.exports = getBoard;