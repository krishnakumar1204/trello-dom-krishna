/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./*.html'],
  theme: {
    extend: {
      colors: {
        primaryColor: '#008de2',
        inputBorderColor: '#82b1ec'
      }
    },
  },
  plugins: [],
}

