import getLists from "../apis/getAllLists.js";
import createAList from "../apis/createAList.js";
import deleteList from "../apis/deleteList.js";
import getCards from "../apis/getAllCards.js";
import deleteCard from "../apis/deleteCard.js";
import createCard from "../apis/createACard.js";
import getBoard from "../apis/getBoardData.js";


const boardId = window.location.href.split("=")[1];

// Showing Board Name
async function showBoardName() {
    const boardData = await getBoard(boardId);
    const boardName = boardData.name;

    const pageTitleElement = document.querySelector("title");
    pageTitleElement.textContent = boardName;

    const boardNameElement = document.createElement('h1');
    boardNameElement.className = "text-white text-3xl my-4";
    boardNameElement.textContent = boardName;

    const boardDetailBox = document.getElementById('boardData');

    boardDetailBox.insertBefore(boardNameElement, document.getElementById('lists'));
}




document.addEventListener("DOMContentLoaded", () => {
    showBoardName();
    showLists();
});



// To show the Lists
async function showLists() {

    const listsParent = document.getElementById("lists");

    const listsData = await getLists(boardId);

    for (let i = 0; i < listsData.length; i++) {

        const listId = listsData[i].id;

        const listContainer = document.createElement('div');
        listContainer.className = "listContainer flex flex-col items-center bg-gray-200 space-y-2 rounded-md h-fit ";
        listContainer.id = listId;

        const listDiv = document.createElement('div');
        listDiv.className = "flex justify-between items-center text-primaryColor font-bold w-64 px-4 py-4";
        listDiv.textContent = listsData[i].name;

        const listDeleteButton = document.createElement('img');
        listDeleteButton.src = "./img/bin.png";
        listDeleteButton.className = "cursor-pointer w-4 h-4";

        listDeleteButton.addEventListener("click", () => {
            listContainer.remove();
            deleteList(listId);
        });

        listDiv.appendChild(listDeleteButton);
        listContainer.appendChild(listDiv);


        showCards(listId);

        const addCardDiv = document.createElement('div');

        addCardDiv.className = "flex flex-col w-11/12"


        const createCardButton = document.createElement('button');
        createCardButton.textContent = "+ Add a card";
        createCardButton.className = "btn rounded-md text-white text-lg bg-primaryColor w-full py-2";



        const cardForm = document.createElement("form");
        cardForm.id = "cardForm";
        cardForm.className = "flex flex-col justify-center items-center space-y-2";
        cardForm.style.display = "none";

        const cardInput = document.createElement("input");
        cardInput.id = "cardInput";
        cardInput.placeholder = "Enter Card Name"
        cardInput.className = "input rounded-md border-2 border-primaryColor w-40 px-2 m-auto";
        cardForm.appendChild(cardInput);

        const addButton = document.createElement('button');
        addButton.textContent = "Add";
        addButton.className = "btn bg-primaryColor text-white px-2 rounded-md";
        cardForm.appendChild(addButton);

        addCardDiv.appendChild(cardForm);

        addCardDiv.appendChild(createCardButton);

        createCardButton.addEventListener('click', () => {
            createNewCard(listId, cardForm, createCardButton, cardInput);
        });

        listContainer.appendChild(addCardDiv);

        listsParent.insertBefore(listContainer, document.getElementById("createListDiv"));
    }
}


// Create new list
const listForm = document.getElementById('listForm');

listForm.addEventListener("submit", (e) => {

    e.preventDefault();

    const listName = document.getElementById("listInput").value;

    if (listName) {

        createAList(listName, boardId);

        setTimeout(() => {
            location.reload();
        }, 500);
    }

});



// To show Cards
async function showCards(listId) {

    const cardsData = await getCards(listId);

    for (let i = 0; i < cardsData.length; i++) {

        const cardDiv = document.createElement('div');
        cardDiv.className = "flex justify-between items-center rounded-md bg-white text-primaryColor w-11/12 h-fit px-2 py-3";
        cardDiv.textContent = cardsData[i].name;

        const cardDeleteButton = document.createElement('img');
        cardDeleteButton.src = "./img/bin.png";
        cardDeleteButton.className = "cursor-pointer w-4 h-4";
        cardDeleteButton.style.display = "none";

        cardDiv.addEventListener('mouseenter', () => {
            cardDeleteButton.style.display = "block";
        })

        cardDiv.addEventListener('mouseleave', () => {
            cardDeleteButton.style.display = "none";
        })

        cardDeleteButton.addEventListener("click", () => {

            cardDiv.remove();
            deleteCard(cardsData[i].id);

        })

        cardDiv.appendChild(cardDeleteButton);



        const cardsParent = document.getElementById(listId);
        cardsParent.insertBefore(cardDiv, cardsParent.lastChild);
    }
}




// Create New Card
function createNewCard(listId, cardForm, createCardButton, cardInput) {
    cardForm.style.display = "flex";
    createCardButton.style.display = "none";

    cardForm.addEventListener("submit", (e) => {
        e.preventDefault();
        const cardName = cardInput.value.trim();

        if (cardName != "") {
            createCard(listId, cardName);
            setTimeout(() => {

                location.reload();
            }, 500);
        }

        cardForm.style.display = "none";
        createCardButton.style.display = "block";
    })
}