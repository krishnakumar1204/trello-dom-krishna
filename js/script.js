import getInitialBoards from "../apis/getInitialBoards.js";
import createABoard from "../apis/createABoard.js";


document.addEventListener("DOMContentLoaded", () => {
    showAllBoards();
});

const boardsParent = document.getElementById("boards");

async function showAllBoards() {

    const boardList = await getInitialBoards();

    for(let i=0; i<boardList.length; i++){
        
        const boardName = boardList[i].name;
        
        const newBoard = document.createElement("a");
        newBoard.textContent = boardName;
        newBoard.className = "flex justify-center items-center bg-primaryColor m-4 w-48 h-24";
        const boardURL = `/board.html?boardId=${boardList[i].id}`;
        newBoard.href = boardURL;
        
        boardsParent.insertBefore(newBoard, document.getElementById("createBoardDiv"));
    }
}


// Filter Box Implementation
const filter = document.getElementById('filter');
filter.addEventListener('keyup', filterBoards);

function filterBoards(e){

    const text = e.target.value.toLowerCase();

    const boards = boardsParent.children;
    Array.from(boards).forEach((board) => {
        if(board.textContent.toLowerCase().indexOf(text) != -1){
            board.style.display = "flex";
        }
        else{
            board.style.display = "none";
        }
    });

}

//Creating New Board
const boardForm = document.getElementById("boardForm");

boardForm.addEventListener("submit", (e) => {

    const boardName = document.getElementById("boardInput").value.trim();

    if (boardName != "") {
        createABoard(boardName);
        setTimeout(() => {
            location.reload();
        }, 500);
    }
});



